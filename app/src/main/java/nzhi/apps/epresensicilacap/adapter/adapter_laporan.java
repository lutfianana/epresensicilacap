package nzhi.apps.epresensicilacap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import nzhi.apps.epresensicilacap.R;
import nzhi.apps.epresensicilacap.model.item_data;

public class adapter_laporan extends RecyclerView.Adapter<adapter_laporan.MyViewHolder> {
    private List<item_data> ListMenu;
    private Context mContext;

    public int getItemViewType(int i) {
        return i;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final LinearLayout LL2;
        public final LinearLayout LLAbsen;
        public final LinearLayout LLbg;
        public final TextView tvjammasuk;
        public final TextView tvjampulang;
        public final TextView tvtanggal;

        public MyViewHolder(View view) {
            super(view);
            this.tvtanggal = view.findViewById(R.id.tvtanggal);
            this.tvjammasuk = view.findViewById(R.id.tvjammasuk);
            this.tvjampulang = view.findViewById(R.id.tvjampulang);
            this.LLAbsen = view.findViewById(R.id.LLabsen);
            this.LLbg = view.findViewById(R.id.LLbg);
            this.LL2 = view.findViewById(R.id.LL2);
        }
    }

    public adapter_laporan(Context context, List<item_data> list) {
        this.mContext = context;
        this.ListMenu = list;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_laporan, viewGroup, false));
    }

    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        item_data item_data = this.ListMenu.get(i);
        if (item_data.getIsLibur().equals("1")) {
            myViewHolder.tvtanggal.setText("Hari Libur");
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) myViewHolder.LL2.getLayoutParams();
            layoutParams.height = 60;
            myViewHolder.LL2.setLayoutParams(layoutParams);
            myViewHolder.LL2.setBackground(this.mContext.getResources().getDrawable(R.drawable.shape_box_libur));
            myViewHolder.tvjammasuk.setText(item_data.getJamDatang());
            myViewHolder.tvjampulang.setText(item_data.getJamPulang());
            myViewHolder.LLbg.setVisibility(View.GONE);
        } else if (item_data.getIsLibur().equals("0")) {
            if (item_data.getJenisMasuk().equals("Finger")) {
                myViewHolder.tvjammasuk.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_absen_finger, 0, 0, 0);
            } else if (item_data.getJenisMasuk().equals("Face")) {
                myViewHolder.tvjammasuk.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_absen_face, 0, 0, 0);
            } else if (item_data.getJenisMasuk().equals("Android")) {
                myViewHolder.tvjammasuk.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_absen_app, 0, 0, 0);
            }
            if (item_data.getJenisPulang().equals("Finger")) {
                myViewHolder.tvjampulang.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_absen_finger, 0, 0, 0);
            } else if (item_data.getJenisMasuk().equals("Face")) {
                myViewHolder.tvjampulang.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_absen_face, 0, 0, 0);
            } else if (item_data.getJenisMasuk().equals("Android")) {
                myViewHolder.tvjampulang.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_absen_app, 0, 0, 0);
            }
            myViewHolder.tvtanggal.setText(item_data.getTanggal());
            myViewHolder.tvjammasuk.setText(item_data.getJamDatang());
            myViewHolder.tvjampulang.setText(item_data.getJamPulang());
        }
    }

    public int getItemCount() {
        return this.ListMenu.size();
    }

    private void setMargins(View view, int i, int i2, int i3, int i4) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).setMargins(i, i2, i3, i4);
            view.requestLayout();
        }
    }
}
