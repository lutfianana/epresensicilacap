package nzhi.apps.epresensicilacap.model;

public class item_data {
    private String Alamat;
    private String IsLibur;
    private String JamDatang;
    private String JamPulang;
    private String JenisAbsen;
    private String JenisMasuk;
    private String JenisPulang;
    private String JumlahAbsen;
    private String JumlahMasuk;
    private String Lat;
    private String LatNow;
    private String Long;
    private String LongNow;
    private String NamaBulan;
    private String NamaLok;
    private String StatusAbsenMasuk;
    private String StatusAbsenPulang;
    private String Tanggal;
    private String jarak;
    private String statVerifikasi;

    public String getIsLibur() {
        return this.IsLibur;
    }

    public String getJumlahAbsen() {
        return this.JumlahAbsen;
    }

    public String getJumlahMasuk() {
        return this.JumlahMasuk;
    }

    public String getNamaBulan() {
        return this.NamaBulan;
    }

    public String getJarak() {
        return this.jarak;
    }

    public String getStatVerifikasi() {
        return this.statVerifikasi;
    }

    public String getLatNow() {
        return this.LatNow;
    }

    public String getLongNow() {
        return this.LongNow;
    }

    public String getJenisAbsen() {
        return this.JenisAbsen;
    }

    public String getTanggal() {
        return this.Tanggal;
    }

    public String getLat() {
        return this.Lat;
    }

    public String getNamaLok() {
        return this.NamaLok;
    }

    public String getLong() {
        return this.Long;
    }

    public String getAlamat() {
        return this.Alamat;
    }

    public String getStatusAbsenMasuk() {
        return this.StatusAbsenMasuk;
    }

    public String getStatusAbsenPulang() {
        return this.StatusAbsenPulang;
    }

    public String getJamDatang() {
        return this.JamDatang;
    }

    public String getJamPulang() {
        return this.JamPulang;
    }

    public String getJenisMasuk() {
        return this.JenisMasuk;
    }

    public String getJenisPulang() {
        return this.JenisPulang;
    }

    public void setNamaBulan(String str) {
        this.NamaBulan = str;
    }

    public void setJenisAbsen(String str) {
        this.JenisAbsen = str;
    }

    public void setLat(String str) {
        this.Lat = str;
    }

    public void setLong(String str) {
        this.Long = str;
    }

    public void setNamaLok(String str) {
        this.NamaLok = str;
    }

    public void setAlamat(String str) {
        this.Alamat = str;
    }

    public void setStatusAbsenMasuk(String str) {
        this.StatusAbsenMasuk = str;
    }

    public void setStatusAbsenPulang(String str) {
        this.StatusAbsenPulang = str;
    }

    public void setJamDatang(String str) {
        this.JamDatang = str;
    }

    public void setJamPulang(String str) {
        this.JamPulang = str;
    }

    public void setTanggal(String str) {
        this.Tanggal = str;
    }

    public void setJenisMasuk(String str) {
        this.JenisMasuk = str;
    }

    public void setJenisPulang(String str) {
        this.JenisPulang = str;
    }

    public void setLatNow(String str) {
        this.LatNow = str;
    }

    public void setLongNow(String str) {
        this.LongNow = str;
    }

    public void setJarak(String str) {
        this.jarak = str;
    }

    public void setStatVerifikasi(String str) {
        this.statVerifikasi = str;
    }

    public void setJumlahAbsen(String str) {
        this.JumlahAbsen = str;
    }

    public void setJumlahMasuk(String str) {
        this.JumlahMasuk = str;
    }

    public void setIsLibur(String str) {
        this.IsLibur = str;
    }
}
