package nzhi.apps.epresensicilacap.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import nzhi.apps.epresensicilacap.BuildConfig;
import nzhi.apps.epresensicilacap.R;
import nzhi.apps.epresensicilacap.helper.UserHelper_sqlite;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3;
    TextView tversi;
    UserHelper_sqlite userHelper_sqlite;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(134217728, 134217728);
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.activity_splash);
        this.tversi = findViewById(R.id.tversi);
        this.userHelper_sqlite = new UserHelper_sqlite(getApplication());
        CheckUPdate();
    }

    private void Delay() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (SplashActivity.this.userHelper_sqlite.getobject("nip") == null) {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this.getApplicationContext(), LoginActivity.class));
                    SplashActivity.this.finish();
                    return;
                }
                SplashActivity.this.startActivity(new Intent(SplashActivity.this.getApplicationContext(), DashboardActivity.class));
                SplashActivity.this.finish();
            }
        }, 3000);
    }

    private void CheckUPdate() {
        try {
            TextView textView = this.tversi;
            textView.setText("Versi " + BuildConfig.VERSION_NAME);
            if (!BuildConfig.VERSION_NAME.equals(new VersionChecker().execute(new String[0]).get())) {

              /* AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((CharSequence) "Peringatan");
                builder.setMessage((CharSequence) "Versi Baru E-Presensi Cilacap Telah Tersedia, Silakan Melakukan Update Terbaru Melalui Playstore Anda");
                builder.setPositiveButton((CharSequence) "UPDATE SEKARANG", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String packageName = SplashActivity.this.getPackageName();
                        try {
                            SplashActivity splashActivity = SplashActivity.this;
                            splashActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + packageName)));
                        } catch (ActivityNotFoundException unused) {
                            SplashActivity splashActivity2 = SplashActivity.this;
                            splashActivity2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                        }
                    }
                });
                builder.show();
                return;*/
              Delay();
            }
            Delay();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    public class VersionChecker extends AsyncTask<String, String, String> {
        private String newVersion;

        public VersionChecker() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... strArr) {
            try {
                this.newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName()).timeout(30000).userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").referrer("http://www.google.com").get().select(".hAyfc .htlgb").get(7).ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.newVersion;
        }
    }
}
