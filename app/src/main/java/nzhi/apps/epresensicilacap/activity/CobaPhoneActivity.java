package nzhi.apps.epresensicilacap.activity;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import nzhi.apps.epresensicilacap.R;

public class CobaPhoneActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int RESOLVE_HINT = 1011;
    GoogleApiClient apiClient;
    TextView ednumber;

    public void onConnected(@Nullable Bundle bundle) {
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void onConnectionSuspended(int i) {
    }

    /* access modifiers changed from: protected */
    @RequiresApi(api = 23)
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_login2);
        this.ednumber = findViewById(R.id.ednotel);
        this.ednumber.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CobaPhoneActivity.this.requestHint();
            }
        });
    }

    /* access modifiers changed from: private */
    public void requestHint() {
        this.apiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).enableAutoManage(this, this).addApi(Auth.CREDENTIALS_API).addOnConnectionFailedListener(this).build();
        this.apiClient.connect();
        try {
            startIntentSenderForResult(Auth.CredentialsApi.getHintPickerIntent(this.apiClient, new HintRequest.Builder().setHintPickerConfig(new CredentialPickerConfig.Builder().setShowCancelButton(true).build()).setPhoneNumberIdentifierSupported(true).build()).getIntentSender(), 1011, null, 0, 0, 0, new Bundle());
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(new String[]{"android.permission.READ_SMS", "android.permission.READ_PHONE_NUMBERS", "android.permission.READ_PHONE_STATE"}, 100);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1011 && i2 == -1) {
            Credential credential = intent.getParcelableExtra(Credential.EXTRA_KEY);
            this.ednumber.setText(credential.getId());
            Log.d("KRED :", credential.getId());
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == 100) {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, "android.permission.READ_SMS") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.READ_PHONE_NUMBERS") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE") == 0) {
                this.ednumber.setText(telephonyManager.getLine1Number());
            }
        }
    }
}
