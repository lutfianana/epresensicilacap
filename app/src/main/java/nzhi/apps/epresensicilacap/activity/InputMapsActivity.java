package nzhi.apps.epresensicilacap.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import nzhi.apps.epresensicilacap.R;
import nzhi.apps.epresensicilacap.bridge.AppConfig;
import nzhi.apps.epresensicilacap.bridge.AppController;
import nzhi.apps.epresensicilacap.helper.GPSTracker;
import nzhi.apps.epresensicilacap.helper.UserHelper_sqlite;
import org.json.JSONException;
import org.json.JSONObject;

public class InputMapsActivity extends FragmentActivity implements OnMapReadyCallback {
    /* access modifiers changed from: private */
    public static String TAG = "MAP LOCATION";
    private String[] Item = {"datang", "pulang"};
    LatLng TPoint;
    String address;
    List<Address> addresses;
    Button btnkirim;
    DateFormat df;
    DateFormat djam;
    EditText edketerangan;
    Geocoder geocoder;
    GPSTracker gpsTracker;
    String knownName;
    Double lat1;
    LatLng latlng;
    Double latnow;
    Double lng1;
    Double longnow;
    /* access modifiers changed from: private */
    public GoogleMap mMap;
    SupportMapFragment mapFragment;
    ProgressDialog pDialog;
    Spinner spjenis;
    String strjam;
    String strjenis;
    String strlat;
    String strlong;
    String strtanggal;
    Date today;
    TextView tvalamatlok;
    TextView tvnamalok;
    UserHelper_sqlite userHelper_sqlite;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_input_lokasiabsen);
        this.pDialog = new ProgressDialog(this);
        this.userHelper_sqlite = new UserHelper_sqlite(getApplication());
        this.gpsTracker = new GPSTracker(getApplicationContext());
        this.df = new SimpleDateFormat("yyy-MM-dd");
        this.djam = new SimpleDateFormat("HH:mm:ss");
        this.today = Calendar.getInstance().getTime();
        this.strtanggal = this.df.format(this.today);
        this.strjam = this.djam.format(this.today);
        this.geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        this.spjenis = findViewById(R.id.spjenis);
        this.tvnamalok = findViewById(R.id.tvnamalokasi);
        this.tvalamatlok = findViewById(R.id.tvalamatlok);
        this.btnkirim = findViewById(R.id.btnkirim);
        this.edketerangan = findViewById(R.id.edketerangan);
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, this.Item);
        this.spjenis.setAdapter(arrayAdapter);
        this.spjenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                InputMapsActivity.this.strjenis = arrayAdapter.getItem(i).toString();
            }
        });
        this.mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        this.mapFragment.getMapAsync(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void showPDialog(String str) {
        if (!this.pDialog.isShowing()) {
            this.pDialog.setMessage(str);
        }
        this.pDialog.show();
    }

    /* access modifiers changed from: private */
    public void hidePDialog() {
        if (this.pDialog.isShowing()) {
            this.pDialog.dismiss();
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        this.lat1 = Double.valueOf(Double.parseDouble(this.userHelper_sqlite.getlonglat("lat_now")));
        this.lng1 = Double.valueOf(Double.parseDouble(this.userHelper_sqlite.getlonglat("long_now")));
        LatLng latLng = new LatLng(this.lat1.doubleValue(), this.lng1.doubleValue());
        try {
            this.addresses = this.geocoder.getFromLocation(this.lat1.doubleValue(), this.lng1.doubleValue(), 1);
            if (this.addresses == null || this.addresses.isEmpty()) {
                this.address = "";
                this.knownName = "";
            } else {
                this.address = this.addresses.get(0).getAddressLine(0);
                this.addresses.get(0).getAddressLine(0);
                this.addresses.get(0).getLocality();
                this.addresses.get(0).getAdminArea();
                this.addresses.get(0).getCountryName();
                this.addresses.get(0).getPostalCode();
                this.knownName = this.addresses.get(0).getFeatureName();
            }
            this.tvnamalok.setText(this.knownName);
            this.tvalamatlok.setText(this.address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mMap.addMarker(new MarkerOptions().position(latLng).title("My Location"));
        this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
        this.mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        this.mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(latLng.latitude + " : " + latLng.longitude);
                InputMapsActivity.this.mMap.clear();
                InputMapsActivity inputMapsActivity = InputMapsActivity.this;
                inputMapsActivity.latlng = latLng;
                inputMapsActivity.lat1 = Double.valueOf(latLng.latitude);
                InputMapsActivity.this.lng1 = Double.valueOf(latLng.longitude);
                InputMapsActivity.this.mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                InputMapsActivity.this.mMap.addMarker(markerOptions);
                try {
                    InputMapsActivity.this.lat1 = Double.valueOf(latLng.latitude);
                    InputMapsActivity.this.lng1 = Double.valueOf(latLng.longitude);
                    InputMapsActivity.this.addresses = InputMapsActivity.this.geocoder.getFromLocation(InputMapsActivity.this.lat1.doubleValue(), InputMapsActivity.this.lng1.doubleValue(), 1);
                    String addressLine = InputMapsActivity.this.addresses.get(0).getAddressLine(0);
                    String locality = InputMapsActivity.this.addresses.get(0).getLocality();
                    String adminArea = InputMapsActivity.this.addresses.get(0).getAdminArea();
                    String countryName = InputMapsActivity.this.addresses.get(0).getCountryName();
                    String postalCode = InputMapsActivity.this.addresses.get(0).getPostalCode();
                    String featureName = InputMapsActivity.this.addresses.get(0).getFeatureName();
                    Log.d("nama :", addressLine + " " + locality + " " + adminArea + " " + countryName + " " + postalCode + " " + featureName);
                    InputMapsActivity.this.tvnamalok.setText(featureName);
                    InputMapsActivity.this.tvalamatlok.setText(addressLine);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.btnkirim.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                InputMapsActivity inputMapsActivity = InputMapsActivity.this;
                inputMapsActivity.latnow = Double.valueOf(inputMapsActivity.gpsTracker.getLatitude());
                InputMapsActivity inputMapsActivity2 = InputMapsActivity.this;
                inputMapsActivity2.longnow = Double.valueOf(inputMapsActivity2.gpsTracker.getLongitude());
                InputMapsActivity.this.SendAbsen("" + InputMapsActivity.this.latnow, "" + InputMapsActivity.this.longnow);
            }
        });
    }

    /* access modifiers changed from: private */
    public void SendAbsen(String str, String str2) {
        showPDialog("Loading ..");
        Log.d("loggedin_token", this.userHelper_sqlite.getobject("loggedin_token"));
        Log.d("NIP", this.userHelper_sqlite.getobject("nip"));
        Log.d("TANGGAL", this.strtanggal);
        Log.d("JAMMM", this.strjam);
        Log.d("LATTT", str);
        Log.d("LONGGG", str2);
        Log.d("KETERANGAN", this.edketerangan.getText().toString());
        Log.d("API_KEY", AppConfig.api_key);
        final String str3 = str;
        final String str4 = str2;
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_SendAbsenManual, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d("RESPON ", str);
                InputMapsActivity.this.hidePDialog();
                try {
                    String string = new JSONObject(str).getString("response");
                    if (string.equals("1")) {
                        InputMapsActivity.this.DialogPesan("Sukses", "Berhasil Input Absen");
                    } else if (string.equals("4")) {
                        InputMapsActivity.this.DialogPesan("Warning", "Maaf, Anda tidak memiliki jadwal Dinas Hari ini");
                    } else {
                        InputMapsActivity.this.DialogPesanGagal("Gagal", "Maaf, Terjadi Kesalahan. Mohon Ulangi");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String access$500 = InputMapsActivity.TAG;
                VolleyLog.d(access$500, "Error: " + volleyError.getMessage());
                InputMapsActivity.this.hidePDialog();
                InputMapsActivity.this.DialogPesan("Error", "Terjadi Kesalahan Input");
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("loggedin_token", InputMapsActivity.this.userHelper_sqlite.getobject("loggedin_token"));
                hashMap.put("nip", InputMapsActivity.this.userHelper_sqlite.getobject("nip"));
                hashMap.put("tanggal", InputMapsActivity.this.strtanggal);
                hashMap.put("jam", InputMapsActivity.this.strjam);
                hashMap.put("lat", str3);
                hashMap.put("long", str4);
                hashMap.put("jenis", InputMapsActivity.this.strjenis);
                hashMap.put("keterangan", InputMapsActivity.this.edketerangan.getText().toString());
                hashMap.put("api_key", AppConfig.api_key);
                hashMap.put(HttpRequest.HEADER_CONTENT_TYPE, "application/json; charset=utf-8");
                hashMap.put("Accept", "application/json");
                return hashMap;
            }
        });
    }

    /* access modifiers changed from: private */
    public void DialogPesan(String str, String str2) {
        new MaterialDialog.Builder(this).title(str).content(str2).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                Intent intent = new Intent(InputMapsActivity.this.getApplicationContext(), DashboardActivity.class);
                intent.putExtra("data", "2");
                InputMapsActivity.this.startActivity(intent);
                InputMapsActivity.this.finish();
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void DialogPesanGagal(String str, String str2) {
        new MaterialDialog.Builder(this).title(str).content(str2).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
            }
        }).show();
    }
}
