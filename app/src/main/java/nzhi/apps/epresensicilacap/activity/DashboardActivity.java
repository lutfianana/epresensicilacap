package nzhi.apps.epresensicilacap.activity;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import nzhi.apps.epresensicilacap.R;
import nzhi.apps.epresensicilacap.fragment.HomeFragment;
import nzhi.apps.epresensicilacap.fragment.LaporanFragment;
import nzhi.apps.epresensicilacap.fragment.PresensiFragment;

public class DashboardActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_dashboard);
        getSupportActionBar().hide();
        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        String stringExtra = getIntent().getStringExtra("data");
        if (stringExtra != null && stringExtra.contentEquals("1")) {
            loadFragment(new HomeFragment());
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
        } else if (stringExtra == null || !stringExtra.contentEquals("2")) {
            loadFragment(new HomeFragment());
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
        } else {
            loadFragment(new PresensiFragment());
            bottomNavigationView.setSelectedItemId(R.id.nav_lokasi);
        }
    }

    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment;
        switch (menuItem.getItemId()) {
            case R.id.nav_gift:
                fragment = new LaporanFragment();
                break;
            case R.id.nav_home:
                fragment = new HomeFragment();
                break;
            case R.id.nav_lokasi:
                fragment = new PresensiFragment();
                break;
            default:
                fragment = null;
                break;
        }
        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment == null) {
            return false;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        return true;
    }
}
