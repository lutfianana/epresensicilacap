package nzhi.apps.epresensicilacap.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import io.fabric.sdk.android.services.common.IdManager;
import java.util.HashMap;
import java.util.Map;
import nzhi.apps.epresensicilacap.R;
import nzhi.apps.epresensicilacap.bridge.AppConfig;
import nzhi.apps.epresensicilacap.bridge.AppController;
import nzhi.apps.epresensicilacap.helper.IMEIUtil;
import nzhi.apps.epresensicilacap.helper.UserHelper_sqlite;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    /* access modifiers changed from: private */
    public static String TAG = "LoginActivity";
    private int REQUEST_PERMISSION_PHONE_STATE = 1;
    Button btnlogin;
    EditText ednip;
    EditText ednotel;
    GoogleApiClient googleApiClient;
    String nama;
    ProgressDialog pDialog;
    String strimei;
    UserHelper_sqlite userHelper_sqlite;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(134217728, 134217728);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(0));
        setJudul("");
        this.userHelper_sqlite = new UserHelper_sqlite(getApplication());
        this.ednip = findViewById(R.id.edNIP);
        this.ednotel = findViewById(R.id.ednotel);
        this.btnlogin = findViewById(R.id.btn_login);
        this.ednotel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        this.pDialog = new ProgressDialog(this);
        showPhoneStatePermission();
        this.btnlogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LoginActivity loginActivity = LoginActivity.this;
                loginActivity.SendLogin(loginActivity.ednip.getText().toString(), LoginActivity.this.ednotel.getText().toString());
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void showPDialog(String str) {
        if (!this.pDialog.isShowing()) {
            this.pDialog.setMessage(str);
        }
        this.pDialog.show();
    }

    /* access modifiers changed from: private */
    public void hidePDialog() {
        if (this.pDialog.isShowing()) {
            this.pDialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void SendLogin(final String str, String str2) {
        showPDialog("Loading ..");
        final String str3 = str;
        final String str4 = str2;
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_Login, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d("RESPON ", str);
                LoginActivity.this.hidePDialog();
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    String string = jSONObject.getString("response");
                    if (string.equals("1")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("data");
                        LoginActivity.this.nama = jSONObject2.getString("nama");
                        String string2 = jSONObject2.getString("nip");
                        String string3 = jSONObject2.getString("foto");
                        String string4 = jSONObject2.getString("skpd");
                        String string5 = jSONObject2.getString("loggedin_token");
                        String string6 = jSONObject2.getString("lat");
                        String string7 = jSONObject2.getString("long");
                        LoginActivity.this.nama = LoginActivity.this.nama.replace("'", "");
                        LoginActivity.this.userHelper_sqlite.saveuser(string2, LoginActivity.this.nama, "", string5, string3, string4, string6, string7);
                        LoginActivity.this.userHelper_sqlite.savelonglatnow(string2, IdManager.DEFAULT_VERSION_NAME, IdManager.DEFAULT_VERSION_NAME);
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this.getApplicationContext(), DashboardActivity.class));
                        LoginActivity.this.finish();
                    } else if (string.equals("13")) {
                        LoginActivity.this.DialogPesanGagal("Login Gagal", "Maaf, Anda tidak diijinkan Login Melalui Perangkat Ini. Silakan Hubungi Admin OPD Anda");
                    } else {
                        LoginActivity loginActivity = LoginActivity.this;
                        loginActivity.DialogPesanGagal("Login Gagal", "Maaf,\nNIP " + str + " /No HP Anda tidak ditemukan dalam database eSimpeg.\n\nAnda bisa menghubungi kantor BKPPD untuk info lebih lanjut");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String access$300 = LoginActivity.TAG;
                VolleyLog.d(access$300, "Error: " + volleyError.getMessage());
                LoginActivity.this.hidePDialog();
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("nip", str3);
                hashMap.put("telp", str4);
                hashMap.put("api_key", AppConfig.api_key);
                hashMap.put("imei", LoginActivity.this.strimei);
                return hashMap;
            }
        });
    }

    private void SendLogin2(String str, String str2) {
        final String str3 = str;
        final String str4 = str2;
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_Login, new Response.Listener<String>() {
            public void onResponse(String str) {
                String access$300 = LoginActivity.TAG;
                Log.e(access$300, "respon 2 : " + str);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String access$300 = LoginActivity.TAG;
                Log.e(access$300, "Error at sign in : " + volleyError.getMessage());
            }
        }) {
            public HashMap<String, String> getParams() {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("nip", str3);
                hashMap.put("telp", str4);
                hashMap.put("api_key", AppConfig.api_key);
                return hashMap;
            }
        });
    }

    private void SendLoginOffline(String str, String str2) {
        startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
        finish();
    }

    private void DialogPesan(String str, String str2) {
        new MaterialDialog.Builder(this).title(str).content(str2).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void DialogPesanGagal(String str, String str2) {
        new MaterialDialog.Builder(this).title(str).content(str2).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
            }
        }).show();
    }

    private void setJudul(String str) {
        getSupportActionBar().setTitle(str);
    }

    private void showPhoneStatePermission() {
        if (ContextCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE") != 0) {
            requestPermission("android.permission.READ_PHONE_STATE", this.REQUEST_PERMISSION_PHONE_STATE);
        } else {
            getIMEI();
        }
    }

    private void getIMEI() {
        this.strimei = IMEIUtil.getDeviceId(this);
    }

    private void requestPermission(String str, int i) {
        ActivityCompat.requestPermissions(this, new String[]{str}, i);
    }

    private void toastMessage(String str) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == this.REQUEST_PERMISSION_PHONE_STATE) {
            showPhoneStatePermission();
        }
    }
}
