package nzhi.apps.epresensicilacap.helper;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class helper {
    private Activity mActivity;
    private Context mContext;

    public static String getNamaBulan(int i) {
        switch (i) {
            case 1:
                return "Januari";
            case 2:
                return "Februari";
            case 3:
                return "Maret";
            case 4:
                return "April";
            case 5:
                return "Mei";
            case 6:
                return "Juni";
            case 7:
                return "Juli";
            case 8:
                return "Agustus";
            case 9:
                return "September";
            case 10:
                return "Oktober";
            case 11:
                return "November";
            case 12:
                return "Desember";
            default:
                return "";
        }
    }

    public helper(Activity activity, Context context) {
        this.mContext = context;
        this.mActivity = activity;
    }

    public helper() {
    }

    public static String getJenisKelamin(String str) {
        return str.equals("P") ? "Pria" : "Wanita";
    }

    public static String getNamaHari(int i, int i2, int i3) {
        return new SimpleDateFormat("EEEE").format(new Date(i, i2, i3 - 1));
    }

    public static String getNamaBulanbyLoc(int i, int i2, int i3) {
        return new SimpleDateFormat("MMM").format(new Date(i, i2, i3));
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x006e A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0071 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0074 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0077 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007a A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x007d A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static String getNameCode(String r2) {
        /*
            int r0 = r2.hashCode()
            r1 = 1536(0x600, float:2.152E-42)
            if (r0 == r1) goto L_0x005d
            r1 = 1543(0x607, float:2.162E-42)
            if (r0 == r1) goto L_0x0053
            r1 = 1545(0x609, float:2.165E-42)
            if (r0 == r1) goto L_0x0049
            r1 = 1570(0x622, float:2.2E-42)
            if (r0 == r1) goto L_0x003f
            r1 = 1576(0x628, float:2.208E-42)
            if (r0 == r1) goto L_0x0035
            r1 = 1600(0x640, float:2.242E-42)
            if (r0 == r1) goto L_0x002b
            r1 = 1824(0x720, float:2.556E-42)
            if (r0 == r1) goto L_0x0021
            goto L_0x0067
        L_0x0021:
            java.lang.String r0 = "99"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0067
            r2 = 6
            goto L_0x0068
        L_0x002b:
            java.lang.String r0 = "22"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0067
            r2 = 5
            goto L_0x0068
        L_0x0035:
            java.lang.String r0 = "19"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0067
            r2 = 4
            goto L_0x0068
        L_0x003f:
            java.lang.String r0 = "13"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0067
            r2 = 3
            goto L_0x0068
        L_0x0049:
            java.lang.String r0 = "09"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0067
            r2 = 2
            goto L_0x0068
        L_0x0053:
            java.lang.String r0 = "07"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0067
            r2 = 1
            goto L_0x0068
        L_0x005d:
            java.lang.String r0 = "00"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0067
            r2 = 0
            goto L_0x0068
        L_0x0067:
            r2 = -1
        L_0x0068:
            java.lang.String r0 = "Unknown error"
            switch(r2) {
                case 0: goto L_0x007d;
                case 1: goto L_0x007a;
                case 2: goto L_0x0077;
                case 3: goto L_0x0074;
                case 4: goto L_0x0071;
                case 5: goto L_0x006e;
                case 6: goto L_0x007f;
                default: goto L_0x006d;
            }
        L_0x006d:
            goto L_0x007f
        L_0x006e:
            java.lang.String r0 = "Data sudah terdaftar"
            goto L_0x007f
        L_0x0071:
            java.lang.String r0 = "Layanan tidak valid"
            goto L_0x007f
        L_0x0074:
            java.lang.String r0 = "Data tidak ditemukan"
            goto L_0x007f
        L_0x0077:
            java.lang.String r0 = "Tidak diijinkan"
            goto L_0x007f
        L_0x007a:
            java.lang.String r0 = "Akun belum aktif"
            goto L_0x007f
        L_0x007d:
            java.lang.String r0 = "Success"
        L_0x007f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: nzhi.apps.epresensicilacap.helper.helper.getNameCode(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static String getStatusBayar(String r3) {
        /*
            int r0 = r3.hashCode()
            r1 = 2
            r2 = 1
            switch(r0) {
                case 48: goto L_0x001e;
                case 49: goto L_0x0014;
                case 50: goto L_0x000a;
                default: goto L_0x0009;
            }
        L_0x0009:
            goto L_0x0028
        L_0x000a:
            java.lang.String r0 = "2"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0028
            r3 = 2
            goto L_0x0029
        L_0x0014:
            java.lang.String r0 = "1"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0028
            r3 = 1
            goto L_0x0029
        L_0x001e:
            java.lang.String r0 = "0"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0028
            r3 = 0
            goto L_0x0029
        L_0x0028:
            r3 = -1
        L_0x0029:
            if (r3 == 0) goto L_0x0038
            if (r3 == r2) goto L_0x0035
            if (r3 == r1) goto L_0x0032
            java.lang.String r3 = "Unknown error"
            goto L_0x003a
        L_0x0032:
            java.lang.String r3 = "Lunas DP"
            goto L_0x003a
        L_0x0035:
            java.lang.String r3 = "Menunggu Verifikasi"
            goto L_0x003a
        L_0x0038:
            java.lang.String r3 = "Belum Terbayar"
        L_0x003a:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: nzhi.apps.epresensicilacap.helper.helper.getStatusBayar(java.lang.String):java.lang.String");
    }

    public static String URLValidasi(String str) {
        if (str.contains("http://") || str.contains("https://")) {
            return str;
        }
        return "http://" + str;
    }

    public static String getRupiah(int i) {
        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        DecimalFormat decimalFormat = (DecimalFormat) currencyInstance;
        DecimalFormatSymbols decimalFormatSymbols = decimalFormat.getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        return "Rp. " + currencyInstance.format((long) i) + ",-";
    }

    public static String DateNormal(String str) {
        return getTahunFromTglIndo(str) + "-" + getBulanFromTglIndo(str) + "-" + getTanggalFromTglIndo(str);
    }

    public static String DateIndo(String str) {
        return getTanggalFromTgl(str) + " " + getNamaBulan(getBulanFromTgl(str)) + " " + getTahunFromTgl(str);
    }

    public static String DateIndoAngka(String str) {
        return getTanggalFromTgl(str) + "-" + getBulanFromTgl(str) + "-" + getTahunFromTgl(str);
    }

    public static String getTanggalIndoFromNormal(String str) {
        if (getTanggalFromTgl(str) < 10 && getBulanFromTgl(str) < 10) {
            return "0" + getTanggalFromTgl(str) + "-0" + getBulanFromTgl(str) + "-" + getTahunFromTgl(str);
        } else if (getTanggalFromTgl(str) < 10) {
            return "0" + getTanggalFromTgl(str) + "-" + getBulanFromTgl(str) + "-" + getTahunFromTgl(str);
        } else if (getBulanFromTgl(str) < 10) {
            return getTanggalFromTgl(str) + "-0" + getBulanFromTgl(str) + "-" + getTahunFromTgl(str);
        } else {
            return getTanggalFromTgl(str) + "-" + getBulanFromTgl(str) + "-" + getTahunFromTgl(str);
        }
    }

    public static int getTahunFromTgl(String str) {
        return Integer.valueOf(str.substring(0, 4)).intValue();
    }

    public static int getBulanFromTgl(String str) {
        return Integer.valueOf(str.substring(5, 7)).intValue();
    }

    public static int getTanggalFromTgl(String str) {
        return Integer.valueOf(str.substring(8, 10)).intValue();
    }

    public static String getTanggalFromTglIndo(String str) {
        return str.substring(0, 2);
    }

    public static String getBulanFromTglIndo(String str) {
        return str.substring(3, 5);
    }

    public static String getTahunFromTglIndo(String str) {
        return str.substring(6, 10);
    }

    public static String isDataKosong(String str) {
        return (str.equals("") || str.equals(null)) ? "-" : str;
    }

    public static String isDataNull(String str) {
        return (str.equals("null") || str.equals(null)) ? "" : str;
    }

    public static String GetNamaBinBinti(String str, String str2, String str3) {
        if (str2.equals("P") || str2.equals("p")) {
            return str + " bin " + str3;
        } else if (str2.equals("W") || str2.equals("w")) {
            return str + " binti " + str3;
        } else {
            return str + " bin/binti " + str3;
        }
    }

    public static void TextStrike(View view, TextView textView, TextView textView2, int i, int i2, int i3) {
        if (i2 == 0) {
            textView.setText(getRupiah(i));
            textView2.setVisibility(8);
            return;
        }
        textView.setText(getRupiah(i));
        textView.setPaintFlags(textView.getPaintFlags() | 16);
        textView.setTextSize((float) i3);
        textView2.setText(getRupiah(i2));
        if (view != null) {
            view.setVisibility(0);
        }
    }
}
