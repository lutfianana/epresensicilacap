package nzhi.apps.epresensicilacap.helper;

import android.annotation.SuppressLint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.Timer;
import java.util.TimerTask;

public class MyLocation {
    boolean gps_enabled = false;
    LocationManager lm;
    LocationListener locationListenerGps = new LocationListener() {
        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onLocationChanged(Location location) {
            MyLocation.this.timer1.cancel();
            MyLocation.this.locationResult.gotLocation(location);
            MyLocation.this.lm.removeUpdates(this);
            MyLocation.this.lm.removeUpdates(MyLocation.this.locationListenerNetwork);
        }
    };
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }

        public void onLocationChanged(Location location) {
            MyLocation.this.timer1.cancel();
            MyLocation.this.locationResult.gotLocation(location);
            MyLocation.this.lm.removeUpdates(this);
            MyLocation.this.lm.removeUpdates(MyLocation.this.locationListenerGps);
        }
    };
    LocationResult locationResult;
    boolean network_enabled = false;
    Timer timer1;

    public static abstract class LocationResult {
        public abstract void gotLocation(Location location);
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001a */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e  */
    @SuppressLint({"MissingPermission"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getLocation(android.content.Context r7, LocationResult r8) {
        /*
            r6 = this;
            r6.locationResult = r8
            android.location.LocationManager r8 = r6.lm
            if (r8 != 0) goto L_0x0010
            java.lang.String r8 = "location"
            java.lang.Object r7 = r7.getSystemService(r8)
            android.location.LocationManager r7 = (android.location.LocationManager) r7
            r6.lm = r7
        L_0x0010:
            android.location.LocationManager r7 = r6.lm     // Catch:{ Exception -> 0x001a }
            java.lang.String r8 = "gps"
            boolean r7 = r7.isProviderEnabled(r8)     // Catch:{ Exception -> 0x001a }
            r6.gps_enabled = r7     // Catch:{ Exception -> 0x001a }
        L_0x001a:
            android.location.LocationManager r7 = r6.lm     // Catch:{ Exception -> 0x0024 }
            java.lang.String r8 = "network"
            boolean r7 = r7.isProviderEnabled(r8)     // Catch:{ Exception -> 0x0024 }
            r6.network_enabled = r7     // Catch:{ Exception -> 0x0024 }
        L_0x0024:
            boolean r7 = r6.gps_enabled
            if (r7 != 0) goto L_0x002e
            boolean r7 = r6.network_enabled
            if (r7 != 0) goto L_0x002e
            r7 = 0
            return r7
        L_0x002e:
            boolean r7 = r6.gps_enabled
            if (r7 == 0) goto L_0x003e
            android.location.LocationManager r0 = r6.lm
            r2 = 0
            r4 = 0
            android.location.LocationListener r5 = r6.locationListenerGps
            java.lang.String r1 = "gps"
            r0.requestLocationUpdates(r1, r2, r4, r5)
        L_0x003e:
            boolean r7 = r6.network_enabled
            if (r7 == 0) goto L_0x004e
            android.location.LocationManager r0 = r6.lm
            r2 = 0
            r4 = 0
            android.location.LocationListener r5 = r6.locationListenerNetwork
            java.lang.String r1 = "network"
            r0.requestLocationUpdates(r1, r2, r4, r5)
        L_0x004e:
            java.util.Timer r7 = new java.util.Timer
            r7.<init>()
            r6.timer1 = r7
            java.util.Timer r7 = r6.timer1
            nzhi.apps.epresensicilacap.helper.MyLocation$GetLastLocation r8 = new nzhi.apps.epresensicilacap.helper.MyLocation$GetLastLocation
            r8.<init>()
            r0 = 20000(0x4e20, double:9.8813E-320)
            r7.schedule(r8, r0)
            r7 = 1
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: nzhi.apps.epresensicilacap.helper.MyLocation.getLocation(android.content.Context, nzhi.apps.epresensicilacap.helper.MyLocation$LocationResult):boolean");
    }

    class GetLastLocation extends TimerTask {
        GetLastLocation() {
        }

        @SuppressLint({"MissingPermission"})
        public void run() {
            MyLocation.this.lm.removeUpdates(MyLocation.this.locationListenerGps);
            MyLocation.this.lm.removeUpdates(MyLocation.this.locationListenerNetwork);
            Location lastKnownLocation = MyLocation.this.gps_enabled ? MyLocation.this.lm.getLastKnownLocation("gps") : null;
            Location lastKnownLocation2 = MyLocation.this.network_enabled ? MyLocation.this.lm.getLastKnownLocation("network") : null;
            if (lastKnownLocation == null || lastKnownLocation2 == null) {
                if (lastKnownLocation != null) {
                    MyLocation.this.locationResult.gotLocation(lastKnownLocation);
                } else if (lastKnownLocation2 != null) {
                    MyLocation.this.locationResult.gotLocation(lastKnownLocation2);
                } else {
                    MyLocation.this.locationResult.gotLocation(null);
                }
            } else if (lastKnownLocation.getTime() > lastKnownLocation2.getTime()) {
                MyLocation.this.locationResult.gotLocation(lastKnownLocation);
            } else {
                MyLocation.this.locationResult.gotLocation(lastKnownLocation2);
            }
        }
    }
}
