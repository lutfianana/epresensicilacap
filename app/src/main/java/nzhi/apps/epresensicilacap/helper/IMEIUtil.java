package nzhi.apps.epresensicilacap.helper;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class IMEIUtil {
    public static String getDeviceId(Context context) {
        String trim = ((TelephonyManager) context.getSystemService("phone")).getDeviceId().trim();
        if (trim == null) {
            trim = Build.SERIAL + "#" + Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        return trim.trim();
    }
}
