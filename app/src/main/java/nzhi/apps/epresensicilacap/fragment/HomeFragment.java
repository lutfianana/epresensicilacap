package nzhi.apps.epresensicilacap.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import nzhi.apps.epresensicilacap.R;
import nzhi.apps.epresensicilacap.activity.DashboardActivity;
import nzhi.apps.epresensicilacap.activity.LoginActivity;
import nzhi.apps.epresensicilacap.bridge.AppConfig;
import nzhi.apps.epresensicilacap.bridge.AppController;
import nzhi.apps.epresensicilacap.helper.UserHelper_sqlite;
import org.json.JSONException;
import org.json.JSONObject;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    /* access modifiers changed from: private */
    public static String TAG = "HomeFragment";
    String des_pulangcepat;
    DateFormat df;
    DateFormat dlokal;
    ProgressDialog pDialog;
    ProgressBar pgbar;
    View rootView;
    String strabsenmasuk;
    String strabsenppulang;
    String strdescmasuk;
    String strdescpulang;
    String strjamkantor;
    String strjamkerjamasuk;
    String strjamkerjapulang;
    String strlibur;
    String strnama;
    String strnip;
    String strterlambatmenit;
    String strtglnow;
    String strtoday;
    String strtotabsen;
    String strtotalpresent;
    String strtothadir;
    String struptd;
    SwipeRefreshLayout swipeRefreshLayout;
    Date tgllokal;
    Date today;
    TextView tvabsenmasuk;
    TextView tvabsenpulang;
    TextView tvdescmasuk;
    TextView tvdescpulang;
    TextView tvjamkerja;
    TextView tvjammasuk;
    TextView tvjampulang;
    TextView tvlogout;
    TextView tvnama;
    TextView tvnip;
    TextView tvtanggalnow;
    TextView tvtotalabsen;
    TextView tvtotalhadir;
    TextView tvuptd;
    UserHelper_sqlite userHelper_sqlite;

    /* access modifiers changed from: private */
    public void DialogPesanGagal(String str, String str2) {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.rootView = layoutInflater.inflate(R.layout.fragment_home, viewGroup, false);
        this.userHelper_sqlite = new UserHelper_sqlite(getActivity().getApplication());
        this.pDialog = new ProgressDialog(getActivity());
        this.df = new SimpleDateFormat("yyyy-MM-dd");
        this.dlokal = new SimpleDateFormat("EEE, dd MMM yyyy");
        this.today = Calendar.getInstance().getTime();
        this.strtglnow = this.df.format(this.today);
        this.strtoday = this.dlokal.format(this.today);
        Log.d("Tanggal: ", this.strtglnow);
        this.tvlogout = this.rootView.findViewById(R.id.tvlogout);
        this.strnip = this.userHelper_sqlite.getobject("nip");
        Log.d("Loggedin :", this.userHelper_sqlite.getobject("loggedin_token"));
        this.tvlogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HomeFragment.this.logout();
            }
        });
        initview();
        ReqHome();
        return this.rootView;
    }

    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void showPDialog(String str) {
        if (!this.pDialog.isShowing()) {
            this.pDialog.setMessage(str);
        }
        this.pDialog.show();
    }

    /* access modifiers changed from: private */
    public void hidePDialog() {
        if (this.pDialog.isShowing()) {
            this.pDialog.dismiss();
        }
    }

    private void initview() {
        this.tvnama = this.rootView.findViewById(R.id.tvnama);
        this.tvnip = this.rootView.findViewById(R.id.tvnip);
        this.tvuptd = this.rootView.findViewById(R.id.tvuptd);
        this.tvtanggalnow = this.rootView.findViewById(R.id.tvtglnow);
        this.tvjamkerja = this.rootView.findViewById(R.id.tvjamkerja);
        this.tvjammasuk = this.rootView.findViewById(R.id.tvjammasuk);
        this.tvjampulang = this.rootView.findViewById(R.id.tvjampulang);
        this.tvabsenmasuk = this.rootView.findViewById(R.id.tvjammasuk);
        this.tvabsenpulang = this.rootView.findViewById(R.id.tvjampulang);
        this.tvdescmasuk = this.rootView.findViewById(R.id.tvdescmasuk);
        this.tvdescpulang = this.rootView.findViewById(R.id.tvdescpulang);
        this.tvtotalhadir = this.rootView.findViewById(R.id.tvtotalhadir);
        this.tvtotalabsen = this.rootView.findViewById(R.id.tvtotalabsen);
        this.pgbar = this.rootView.findViewById(R.id.viewprogress);
        this.swipeRefreshLayout = this.rootView.findViewById(R.id.swipe_container);
        this.swipeRefreshLayout.setOnRefreshListener(this);
        this.swipeRefreshLayout.setColorSchemeColors(getContext().getResources().getColor(17170453), getContext().getResources().getColor(17170455), getContext().getResources().getColor(17170451), getContext().getResources().getColor(17170457));
    }

    private void ReqHome() {
        showPDialog("Loading ..");
        AppController.getInstance().addToRequestQueue(new StringRequest(1, AppConfig.URL_ReqHome, new Response.Listener<String>() {
            public void onResponse(String str) {
                Log.d("RESPON ", str);
                HomeFragment.this.hidePDialog();
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    String string = jSONObject.getString("response");
                    if (string.equals("1")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("data");
                        HomeFragment.this.strlibur = jSONObject2.getString("libur");
                        if (HomeFragment.this.strlibur.equals("0")) {
                            HomeFragment.this.strabsenmasuk = jSONObject2.getString("jam_datang");
                            HomeFragment.this.strabsenppulang = jSONObject2.getString("jam_pulang");
                            HomeFragment.this.strjamkerjamasuk = jSONObject2.getString("jam_kerja_datang");
                            HomeFragment.this.strjamkerjapulang = jSONObject2.getString("jam_kerja_pulang");
                            HomeFragment.this.strtothadir = jSONObject2.getString("hadir");
                            HomeFragment.this.strtotabsen = jSONObject2.getString("tanpa_keterangan");
                            double parseDouble = Double.parseDouble(jSONObject2.getString("pulangcepat_menit"));
                            DecimalFormat decimalFormat = new DecimalFormat("#.##");
                            HomeFragment.this.des_pulangcepat = decimalFormat.format(parseDouble);
                            double parseDouble2 = Double.parseDouble(jSONObject2.getString("terlambat_menit"));
                            new DecimalFormat("#.##");
                            HomeFragment.this.strterlambatmenit = decimalFormat.format(parseDouble2);
                        } else {
                            HomeFragment.this.strjamkerjamasuk = "00.00";
                            HomeFragment.this.strjamkerjapulang = "00.00";
                            HomeFragment.this.strabsenmasuk = "00.00";
                            HomeFragment.this.strabsenppulang = "00.00";
                            HomeFragment.this.strtothadir = jSONObject2.getString("hadir");
                            HomeFragment.this.strtotabsen = jSONObject2.getString("tanpa_keterangan");
                            new DecimalFormat("#.##");
                            HomeFragment.this.des_pulangcepat = "0";
                            HomeFragment.this.strterlambatmenit = "0";
                        }
                        TextView textView = HomeFragment.this.tvjamkerja;
                        textView.setText("Jam Kerja :" + HomeFragment.this.strjamkerjamasuk + " - " + HomeFragment.this.strjamkerjapulang + " WIB");
                        HomeFragment.this.tvnama.setText(HomeFragment.this.userHelper_sqlite.getobject("nama"));
                        TextView textView2 = HomeFragment.this.tvnip;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Nip. ");
                        sb.append(HomeFragment.this.userHelper_sqlite.getobject("nip"));
                        textView2.setText(sb.toString());
                        HomeFragment.this.tvuptd.setText(HomeFragment.this.userHelper_sqlite.getobject("skpd"));
                        HomeFragment.this.tvtanggalnow.setText(HomeFragment.this.strtoday);
                        HomeFragment.this.tvjammasuk.setText(HomeFragment.this.strabsenmasuk);
                        HomeFragment.this.tvjampulang.setText(HomeFragment.this.strabsenppulang);
                        TextView textView3 = HomeFragment.this.tvdescmasuk;
                        textView3.setText("Terlambat " + HomeFragment.this.strterlambatmenit + " menit");
                        TextView textView4 = HomeFragment.this.tvdescpulang;
                        textView4.setText("Terlalu Awal  " + HomeFragment.this.des_pulangcepat + " menit");
                        HomeFragment.this.tvtotalhadir.setText(jSONObject2.getString("hadir"));
                        HomeFragment.this.tvtotalabsen.setText(jSONObject2.getString("tanpa_keterangan"));
                        int parseInt = Integer.parseInt(jSONObject2.getString("hadir"));
                        Integer.parseInt(jSONObject2.getString("tanpa_keterangan"));
                        int parseInt2 = Integer.parseInt(jSONObject2.getString("total"));
                        if (parseInt2 == 0) {
                            HomeFragment.this.pgbar.setProgress(0);
                        } else {
                            HomeFragment.this.pgbar.setProgress((parseInt * 100) / parseInt2);
                        }
                    } else if (string.equals("9")) {
                        HomeFragment.this.userHelper_sqlite.delete("user_member");
                        HomeFragment.this.startActivity(new Intent(HomeFragment.this.getActivity(), LoginActivity.class));
                        HomeFragment.this.getActivity().finish();
                    } else {
                        HomeFragment homeFragment = HomeFragment.this;
                        homeFragment.DialogPesanGagal("Gagal", "Maaf,\nNIP " + HomeFragment.this.strnip + " tidak ditemukan dalam database eSimpeg.\n\nAnda bisa menghubungi kantor BKPPD untuk info lebih lanjut");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    HomeFragment.this.DialogPesanGagal("Gagal", "Maaf, Terjadi Kesalahan Pengambilan Data, Tarik ke bawah Untuk Refresh");
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                String access$300 = HomeFragment.TAG;
                VolleyLog.d(access$300, "Error: " + volleyError.getMessage());
                HomeFragment.this.hidePDialog();
                HomeFragment.this.DialogPesanGagal("Gagal", "Maaf, Terjadi Kesalahan Pengambilan Data, Tarik ke bawah Untuk Refresh");
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                HashMap hashMap = new HashMap();
                hashMap.put("nip", HomeFragment.this.strnip);
                hashMap.put("tanggal", HomeFragment.this.strtglnow);
                hashMap.put("loggedin_token", HomeFragment.this.userHelper_sqlite.getobject("loggedin_token"));
                hashMap.put("api_key", AppConfig.api_key);
                return hashMap;
            }
        });
    }

    /* access modifiers changed from: private */
    public void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Apakah anda yakin ingin keluar?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                HomeFragment.this.userHelper_sqlite.delete("user_member");
                HomeFragment.this.startActivity(new Intent(HomeFragment.this.getActivity(), LoginActivity.class));
                HomeFragment.this.getActivity().finish();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.create().show();
    }

    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(HomeFragment.this.getActivity(), DashboardActivity.class);
                intent.putExtra("data", "1");
                HomeFragment.this.startActivity(intent);
                HomeFragment.this.getActivity().finish();
            }
        }, 1500);
    }
}
